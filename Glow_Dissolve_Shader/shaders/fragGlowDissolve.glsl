#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

// Image to render
uniform sampler2D texture;

// Current texture coordinate.
// Note that Processing stores the texture
// coordinates in the first two components
// (x and y). No idea what's in the other
// two, but if porting this to another
// framework or plain OpenGL, then you can
// just make this a vec2 and name it
// gl_TexCoord0 or whatever you like.
varying vec4 vertTexCoord;

// Interpolated vertex colour
varying vec4 vertColor;

// The edge threshold for the fading effect.
// Result alpha values between 0 and fade
// are interpolated, values above fade are
// opaque and values below 0 are clipped.
uniform float fade = 0.07;

// The alpha value of the original image to
// keep. This can give a nice glow effect,
// but you can easily switch to a regular
// dissolve by setting this to 0.
uniform float baseAlpha = 0.3;

void main() {
    // Current pixel of the image
    vec4 pixel = texture2D(texture, vertTexCoord.xy);

    // The color of the pixel tinted with the vertex color
    vec4 tintedPixel = pixel * vertColor;

    // The tinted pixel color with its max alpha set to baseAlpha.
    // Remove this variable entirely and you'll have a normal
    // dissolve shader.
    vec4 basePixel = vec4(tintedPixel.rgb, tintedPixel.a * baseAlpha);

    // The brightness of the pixel in this image. A lot of dissolve
    // shaders use another image as a mask for this step. I'm just
    // doing it this way because it involves no extra image
    // parameters and it dissolves the image in a way that will more
    // likely fit with its appearance
    float brightness = (pixel.r + pixel.g + pixel.b)/3 * pixel.a;

    // The threshold used to clip the image's alpha. Values higher
    // than this are kept while values lower than this are discarded.
    float threshold = 1. - vertColor.a;

    // Here's where the magic happens: this calculates the alpha of
    // the image based on the threshold subtracted from the
    // brightness of the image and clipped between 0 and 1.
    float alpha = clamp(brightness - threshold, 0., 1.);

    // Just to prevent a division by zero in case fade is set
    // to 0
    float clampedFade = clamp(fade, 0.00001, 1.);

    // Here we apply the fading effect. If you wanted to apply a
    // glow effect, you can use anther color when alpha's new
    // value is between 0 and 1.
    alpha = clamp(alpha/clampedFade, 0., 1.);

    // Now we use the alpha to interpolate between the base
    // color and the tinted image's color
    gl_FragColor = mix(basePixel, tintedPixel, alpha);
}