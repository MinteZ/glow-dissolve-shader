import com.hamoid.*;

/**
 * Created by Mintesno Zewdu
 */

ArrayList<Particle> particles = new ArrayList();

boolean recording = false;
VideoExport ve;

PShader s;

boolean additive = true;

ArrayList<PImage> particleImages = new ArrayList();
PImage particleImage;

int index = 0;

void settings() {
  size(1280, 720, P2D);
  noSmooth();
}

void setup() {
  File[] imageFiles = listFiles("particles");
  for (int i = 0; i<imageFiles.length; i++) {
    String s = imageFiles[i].getAbsolutePath();
    particleImages.add(loadImage(s));
  }
  setImage(0);

  s = loadShader("shaders/fragGlowDissolve.glsl");
  s.set("fade", 0.13f);
  s.set("baseAlpha", 0.3f);

  ve = new VideoExport(this, "videos/GlowDissolveShader.mp4");
  ve.setFrameRate(60f);
  ve.setQuality(100, 0);
  if (recording) {
    ve.startMovie();
  }

  textFont(createFont("Arial", 32f));
  imageMode(CENTER);
}

void draw() {
  background(0);

  // Rendering a comparison with and without the
  // dissolve shader
  float f = sin(radians(frameCount % 180));
  tint(255f, f * 255);
  image(particleImage, width / 2f - 64, height/2f, 128f, 128f);
  shader(s);
  image(particleImage, width / 2f + 64, height/2f, 128f, 128f);
  resetShader();

  if (frameCount % 2 == 0) {
    particles.add(
      new Particle(
      width / 4f,
      height / 1f,
      -HALF_PI + radians(random(-4f, 4f)),
      random(190f, 240f),
      random(-PI * 0.3f, PI * 0.3f),
      random(40f, 800f),
      3f,
      Gradients.WATER
      )
      );
  }

  for (int i = particles.size() - 1; i >= 0; i--) {
    Particle p = particles.get(i);
    if (p.isDone()) {
      particles.remove(i);
    } else {
      p.update(1f / 60);
      for (int j = 0; j<2; j++) {
        // Render each particle twice, once with
        // the shader and once without
        p.draw(j == 0);
      }
    }
  }

  textAlign(CENTER, TOP);
  fill(255f);
  textSize(32f);
  text("Original shader", width / 4f, 80f);
  text("Custom dissolve shader", width / 4f * 3, 80f);

  if (recording) {
    ve.saveFrame();
  }

  // To keep track of the video recording time independent
  // of frame rate
  if (recording) {
    float seconds = frameCount/60f;
    textSize(16f);
    text(floor(seconds/60) + ":" + nf(floor(seconds), 2), 30, 30);
  }
}

void keyReleased() {
  switch(keyCode) {
  case LEFT:
    setImage(nmod(index - 1, particleImages.size()));
    break;
  case RIGHT:
    setImage(nmod(index + 1, particleImages.size()));
    break;
  }
}

void setImage(int index) {
  this.index = index;
  particleImage = particleImages.get(index);
}

// A Python-like modulus operation that cycles
// around for negative numbers as well
int nmod(int x, int y) {
  return ((x % y) + y) % y;
}

class Particle {

  float x; 
  float y; 
  float angle; 
  float speed;
  float rotationSpeed; 
  float size;
  float duration;
  color[] gradient;  

  public Particle(float x, float y, float angle, float speed, float rotationSpeed, float size, float duration, color[] gradient) {
    this.x = x; 
    this.y = y; 
    this.angle = angle; 
    this.speed = speed;
    this.rotationSpeed = rotationSpeed; 
    this.size = size;
    this.duration = duration;
    this.gradient = gradient;

    velX = cos(angle) * speed;
    velY = sin(angle) * speed;
  }

  float t = 0f;

  float velX;
  float velY;

  boolean isDone() {
    return t >= 1f;
  }

  private color getColor(float percent) {
    percent = constrain(percent, 0, 1);
    float i1 = percent * (gradient.length - 1);
    int i2 = (int)(i1 + 1) % gradient.length;
    return lerpColor(gradient[(int)i1], gradient[i2], i1 % 1f);
  }
  void update(float delta) {
    t += delta / duration;

    x += velX * delta;
    y += velY * delta;
  }

  void draw(boolean useShader) {
    pushStyle();
    if (additive) {
      blendMode(ADD);
    }
    if (useShader) {
      shader(s);
    } else {
      resetShader();
    }
    pushMatrix();
    // If useShader is true, draw this particle
    // with its position reflected horizontally
    //across the middle of the screen
    translate(useShader ? width - x : x, y);
    rotate(t * duration * rotationSpeed);
    tint(getColor(t), (1 - t) * 255);
    image(particleImage, 0f, 0f, size * t, size * t);
    popMatrix();
    popStyle();
  }
}
