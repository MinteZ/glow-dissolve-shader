static class Gradients {
  
  // You can add more gradients here and
  // supply them as an argument to
  // Particle's constructor
  
  static final color[] FIRE = { 
    #ffffff,
    #ffff00,
    #ff8000,
    #ff0000,
    #000000
  };
  
  static final color[] SUNSET = { 
    #fff010,
    #ff7b00,
    #ff0526,
    #ad0022
  };
  
  static final color[] WATER = {
    #181c33,
    #46397f,
    #6868ad,
    #59cbe5,
    #91fffb
  };

}
